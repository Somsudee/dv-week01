var rl = require('readline-sync');

var arr = [];

var row = rl.questionInt('Please input row ');
var col = rl.questionInt('Please input column ');

var i = rl.questionInt('Please input number of array ');
for(var a = 0 ; a < i; a++) {
    arr[a] = rl.questionInt('Please input no '); 
}

var x = '';
for(var j = 0; j < i; j++) {
    x = x + arr[j] + ' ';
}
console.log('\nNumber of rows: '+row +'\n' + 'Number of columns: ' + col);
console.log('Enter data, row by row: \n' + x);


counter = 0;
result = [];
for(var x = 0; x < row; x++) {
    var temp = [];
    for(var j = 0; j < col; j++) {
        if(counter < i ){
            temp.push(arr[counter]);
            counter++;
        }
    }
    result.push(temp);
}


var i = 0;
var j = 0;
var rowSum = 0;
var total = 0;
while (i < row){
    if(i==0){
        while(j < col){
            rowSum += result[i][j];
            j++;   
        }
        var x = '';
        for(var y = 0; y < j; y++) {
            x += result[i][y] + ' ';
        }
        total += rowSum;
        console.log('\nData including row and column total: \n' + x + ' | ' + rowSum);

    } else {
        j = 0;
        rowSum = 0;
        while(j < col){
            rowSum += result[i][j];
            j++;
        }
        var x = '';
        for(var y = 0; y < j; y++) {
            x += result[i][y] + ' ';
        }
        total += rowSum;
        console.log(x + ' | ' + rowSum);
    }
    i++;
}

var i = 0;
var j = 0;
var colSum = 0;
var z = '';
while(j < col){
    if(j==0){
        while(i < row){
            colSum += result[i][j];
            i++; 
        }
        z += colSum + ' ';
    }else {
        i = 0;
        colSum = 0;
        while(i < row){
            colSum += result[i][j];
            i++; 
        }
        z += colSum + ' ';
    }
    
    j++;
}
console.log('\n' + z + ' | ' + total);