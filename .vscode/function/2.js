var rl = require('readline-sync');

console.log(factorial(5));

function factorial(num){
    var total = 1;

    while(num > 1 ) {
        total = total * num;
        num = num - 1;
    }
    return total;
}
