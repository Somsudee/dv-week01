var rl = require('readline-sync');
var num = rl.questionInt('Input number');

var total = 1;

while(num > 1 ) {
    total = total * num;
    num = num - 1;
}

console.log(total);