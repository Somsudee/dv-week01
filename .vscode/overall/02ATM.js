var rl = require('readline-sync');

var initial = 0;
var history = [];

do{
    console.log('1 = Input initial money ');
    console.log('2 = Withdraw money');
    console.log('3 = Deposite money');
    console.log('4 = View the history transaction');
    console.log('0 = exit');

    var menu = rl.questionInt('Please input number of menu ');
    
    switch(menu) {
        case 1:
            initial = rl.questionInt('Input your initial balance ');
            console.log(initial);

            break;

        case 2:
            var withdraw = rl.questionInt('Input withdraw money ');
            initial -= withdraw;
            history.push('Withdraw: ' + withdraw);
            console.log(initial);

            break;

        case 3:
            var deposit = rl.questionInt('Input deposite money ');
            initial += deposit;
            history.push('Deposit: ' + deposit);
            console.log(initial);

            break;

        case 4:
            console.log();
            for(var i = 0; i < history.length; i++){
                console.log(i+1 + '. ' +history[i]);
            }
            console.log();
            break;

        case 0:
            break;
    }
}while(menu != 0);



