var rl = require('readline-sync');

var things = [[10,1.25,'Chocolate'],[10,0.75,'Water'],[10,0.90,'Chips']];
var history = [];

do{
    console.log('1 = Buy things ');
    console.log('2 = Add things ');
    console.log('3 = History of selling and adding the items');
    console.log('0 = exit');

    var menu = rl.questionInt('Please input number of menu ');
    
    switch(menu) {
        case 1:
            console.log('1 = Chocolate $1.25 ');
            console.log('2 = Water $0.75 ');
            console.log('3 = Chips $0.90 ');
            console.log('0 = Stop order ');

            var numItem = [];
            var cost = 0;
            var j = 10;

            //insert item
            for(var i = 0 ; i < j ; i++) {
                var numThing = 0;
                numThing = rl.questionInt('Please input no. '); 
                
                if(numThing > 0 && numThing < 4) {
                    numItem.push(numThing);
                    cost += things[numThing-1][1];
                    things[numThing-1][0] = things[numThing-1][0]-1;
                    history.push('Selling:\t'+things[numThing-1][2] + ' - '+ things[numThing-1][0]);

                    console.log('amount needed to pay: ' + cost);
                }else if( numThing == 0 ){
                    i = j;
                    console.log('Ending ordering... ');
                }else if( i == 10 ){
                    console.log('You ordering too much');
                }
            }
            
            //user pay money
            console.log('\namount needed to pay: ' + cost);

            var nameItem = '';
            for(var i = 0; i < j;i++){
                var paid = rl.questionFloat('Please input your money ');
                
                cost = paid - cost;
                if(cost >= 0 ){
                    console.log('Thank you for buying item: ');
                    for(var i =0; i < numItem.length; i++){
                        var name = things[numItem[i]-1][2];
                        nameItem += name + ' ';
                    }
                    console.log(nameItem);
                    if( cost > 0 ){
                        console.log('Your change is ' + cost.toFixed(2));
                    }
                    i = j;
                }else if( cost < 0 ){
                    result = cost + (cost * -2);
                    
                    console.log('Please insert another ' + result);
                }
            }
            break;

        case 2:
            console.log('1 = Chocolate ');
            console.log('2 = Water');
            console.log('3 = Chips');
            console.log('0 = Stop add items ');

            var j = 10;
            for(var i = 0 ; i < j ; i++) {
                var numThing = rl.questionInt('Please input no. to choose item '); 

                if(numThing > 0 && numThing < 4) {
                    var addItem = rl.questionInt('Please input no. to add item '); 
                    things[numThing-1][0] = things[numThing-1][0] + addItem;
                    history.push('Adding:\t'+things[numThing-1][2] + ' - '+ things[numThing-1][0]);

                    console.log('You added '+ things[numThing-1][2]+ ': ' + things[numThing-1][0]);
                }else if( numThing == 0 ){
                    i = j;
                    console.log('Ending adding... ');
                }else if( i == 10 ){
                    console.log('You adding too much');
                }
            }
            
            console.log();
            console.log('Your items list: ');
            for(var i = 0; i <things.length; i++){
                console.log('   '+things[i][2] + ':\t' + things[i][0]);
            }
            console.log();
            break;
        
        case 3:
            console.log('HISTORY:');
            for(var i = 0; i < history.length; i++){
                console.log(i+1 + '. ' +history[i]);
            }
            console.log();
            break;

        case 0:
            break;

    }
}while( menu !=0 );